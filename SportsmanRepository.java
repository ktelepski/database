package com.mycompany.baza;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class SportsmanRepository {
  
    private static SessionFactory factory;
    
    public SportsmanRepository(SessionFactory factory){
        this.factory = factory;
    }
    
    public Sportsman save(String fname, String lname, String disc, int age, Club club) {
        
        Session session = factory.openSession();
        
        Transaction trans = session.beginTransaction();
        
        Sportsman sportsman = new Sportsman(fname, lname, disc, age);
        sportsman.setClub(club);
        
        session.save(sportsman);
        
        System.out.println("Sportsman " + sportsman.getLastName() + " added to the database");
        
        trans.commit();
        
        System.out.println("SAVED \n");
        
        session.close();
        
        return sportsman;
    }
    
    public void delete(Sportsman sportsman) {
        Session session = factory.openSession();
        
        Transaction trans = session.beginTransaction();
        
        sportsman = (Sportsman)session.get(Sportsman.class, sportsman.getId());
        
        session.delete(sportsman);
        
        System.out.println("Sportsman " + sportsman.getLastName() + " deleted from the database");
        
        trans.commit();
        
        System.out.println("SAVED \n");
        
        session.close();
    }
    
    public void listAll() {
        Session session = factory.openSession();
        Transaction trans = session.beginTransaction();
        
        List<Sportsman> sportsmen = findAll(); 
        
        for(Sportsman sportsman: sportsmen)
        {            
            System.out.println(sportsman);       
        }
        
        trans.commit();
        session.close();
    }
    
    public List findAll() {
        Session session = factory.openSession();
        Transaction trans = session.beginTransaction();
        
        List sportsmen = session.createQuery("FROM Sportsman").list();
        
        trans.commit();
        session.close();
        
        return sportsmen;
    }
}


