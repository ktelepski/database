package com.mycompany.baza;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity (name = "Sportsman")
@Table (name = "Sportsman")
public class Sportsman implements Serializable {
    
    public Sportsman(){}
    public Sportsman(String fname, String lname, String disc, int age) {
        this.firstName = fname;
        this.lastName = lname;
        this.discipline = disc;
        this.age = age;
    }
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column (name = "sportsmanId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column (name = "firstName")
    private String firstName;
    @Column (name = "lastName")
    private String lastName;
    @Column (name = "discipline")
    private String discipline;
    @Column (name = "age")
    private int age;
    //@Column (name = "clubId")
    //private Club club;
    
    @ManyToOne
    @JoinColumn(name = "clubId", referencedColumnName = "clubId")
    private Club club;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getDiscipline() {
        return discipline;
    }
    public void setDiscipline(String disc) {
        this.discipline = disc;
    }
    
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    
    public Club getClub() {
        return club;
    }
    
    public void setClub(Club club) {
        this.club = club;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sportsman)) {
            return false;
        }
        Sportsman other = (Sportsman) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
 
        return firstName + " " + lastName + ", ID = " + id + "\n" 
                + "Dyscyplina: " + discipline + "\n"
                + club + "\n";
    }
    
    }
    
