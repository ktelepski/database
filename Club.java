package com.mycompany.baza;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity (name = "Club")
@Table (name = "Club")
public class Club implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column (name = "clubId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long clubId;
    @Column (name = "name")
    private String name;
    @Column (name = "city")
    private String city;
    @Column (name = "establishmentYear")
    private int establishmentYear;
   
    public Club() {}
    public Club(String name, String city, int establishmentYear) {
        this.name = name;
        this.city = city;
        this.establishmentYear = establishmentYear;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    
    public int getEstablishmentYear() {
        return establishmentYear;
    }
    public void setEstablishmentYear(int establishmentYear) {
        this.establishmentYear = establishmentYear;
    }

    public Long getClubId() {
        return clubId;
    }

    public void setClubId(Long id) {
        this.clubId = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (clubId != null ? clubId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Club)) {
            return false;
        }
        Club other = (Club) object;
        if ((this.clubId == null && other.clubId != null) || (this.clubId != null && !this.clubId.equals(other.clubId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        
        return "ID klubu = " + clubId;
    }
    
}
