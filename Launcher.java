package com.mycompany.baza;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Kacper
 */
public class Launcher {
    
    
    
    public static void main(String[] args)
    {
        SessionFactory factory = SessionFactoryProvider.getInstance();
        
        /*Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        factory = cfg.buildSessionFactory();
        */
        
        
        
        SportsmanRepository sr = new SportsmanRepository(factory);
        ClubRepository cr = new ClubRepository(factory);
        
        Club mmks = cr.save("MMKS Gdańsk", "Gdańsk", 2004);
        Club noClub = cr.save("No club", "None", 0);
        Club nsdap = cr.save("NSDAP", "Berlin", 1920);
        
        Sportsman adolf = sr.save("Adolf", "Hitlur", "Biathlon", 87, nsdap);
        Sportsman roman = sr.save("Roman", "Kostka", "Boxing", 33, noClub);
        Sportsman kacper = sr.save("Kacper", "Telepski", "Kickboxing", 22, mmks);
        
        sr.listAll();
        
        sr.delete(roman);
        
        sr.listAll();
        
    }
}
