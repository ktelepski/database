package com.mycompany.baza;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Kacper
 */
public class ClubRepository {
    
    SessionFactory factory;
    
    public ClubRepository(SessionFactory factory) {
        this.factory = factory;
    }
    
    public Club save(String name, String city, int establishmentYear) {
        
        Session session = factory.openSession();
        
        Transaction trans = session.beginTransaction();
        Club club = new Club(name, city, establishmentYear);
        
        session.save(club);
        
        System.out.println("Club " + club.getName() + " added to the database");
        
        trans.commit();
        session.close();
        
        return club;
    }
    
    public List findAll() {
        Session session = factory.openSession();
        Transaction trans = session.beginTransaction();
        
        List clubs = session.createQuery("FROM Club").list();
        
        trans.commit();
        session.close();
        
        return clubs;
    }
    
    public void listAll() {
        Session session = factory.openSession();
        Transaction trans = session.beginTransaction();
        
        List<Club> clubs = findAll();
        
        for(Club club: clubs)
        {
            System.out.println(club);
        }
        
        trans.commit();
        session.close();
    }
}
