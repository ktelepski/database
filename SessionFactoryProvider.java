/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.baza;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class SessionFactoryProvider {
    
    private static SessionFactory factory;
    
    private SessionFactoryProvider(){ }
    
    public static SessionFactory getInstance(){
        if (factory == null) {
            factory = createSessionFactory();
            return factory;
        }
        else return factory;
    }
    
    private static SessionFactory createSessionFactory(){
        SessionFactory factory;
        
        Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
        factory = cfg.buildSessionFactory();
        
        return factory;
    }
    

}
